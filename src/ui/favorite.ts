import { createElement } from "../helpers/domHelper";
import { createMovieComponent } from "./movie";

export function renderFavorite(movies: any[]): void {
    const offcanvas = createElement({
        tagName: 'div',
        className: 'offcanvas offcanvas-end',
        attributes: {
            'tabindex': '-1',
            'id': 'offcanvasRight',
            'aria-labelledby': 'offcanvasRightLabel'
        }
    });

    const offcanvasHeader = createElement({
        tagName: 'div',
        className: 'offcanvas-header'
    });

    const offcanvasRightLabel = createElement({
        tagName: 'h5',
        attributes: {
            id: 'offcanvasRightLabel'
        }
    });

    offcanvasRightLabel.innerText = 'FAVORITE';

    const offcanvasCloseButton = createElement({
        tagName: 'button',
        className: 'btn-close text-reset',
        attributes: {
            'data-bs-dismiss': 'offcanvas',
            'aria-label': 'Close'
        }
    });

    const favoriteMoviesDiv = createElement({
        tagName: 'div',
        className: 'offcanvas-body',
        attributes: {
            id: 'favorite-movies'
        }
    })

    const moviesComponents = movies.map(movie => createMovieComponent(movie, 'col-12 p-2'));

    offcanvas.appendChild(offcanvasHeader);
    offcanvasHeader.appendChild(offcanvasRightLabel);
    offcanvasHeader.appendChild(offcanvasCloseButton);
    offcanvas.appendChild(favoriteMoviesDiv);

    moviesComponents.forEach(movieComponent => favoriteMoviesDiv.appendChild(movieComponent));

    document.body.appendChild(offcanvas);
}