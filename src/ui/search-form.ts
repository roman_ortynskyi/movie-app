import { createElement } from "../helpers/domHelper";

export function renderSearchForm(): HTMLElement {
    const container = createElement({
        tagName: 'div',
        className: 'container-fluid d-flex bg-light justify-content-center'
    });

    const form = createElement({
        tagName: 'form',
        className: 'form-inline col-6 px-2 d-flex'
    });

    const input = createElement({
        tagName: 'input',
        className: 'form-control m-2',
        attributes: {
            placeholder: 'Search',
            id: 'search',
            name: 'query'
        }
    });

    const button = createElement({
        tagName: 'button',
        className: 'btn btn-dark m-2',
        attributes: {
            id: 'submit',
            type: 'submit'
        }
    });

    button.innerText = 'Search';

    container.appendChild(form);
    form.appendChild(input);
    form.appendChild(button);

    document.body.appendChild(container);

    return form;
}