import { createElement } from "../helpers/domHelper"

export function renderHeader(): void {
    const header = createElement({
        tagName: 'header'
    });

    const navbar = createElement({
        tagName: 'div',
        className: 'navbar navbar-dark bg-dark shadow-sm'
    });

    const container = createElement({
        tagName: 'div',
        className: 'container'
    });

    const logo = createElement({
        tagName: 'a',
        className: 'navbar-brand d-flex align-items-center',
        attributes: {
            href: '#'
        }
    });

    logo.innerText = 'MOVIE';

    const toggler = createElement({
        tagName: 'button',
        className: 'navbar-toggler',
        attributes: {
            'data-bs-toggle': 'offcanvas',
            'data-bs-target': '#offcanvasRight',
            'aria-controls': 'offcanvasRight'
        }
    });

    const togglerIcon = createElement({
        tagName: 'span',
        className: 'navbar-toggler-icon'
    });

    header.appendChild(navbar);
    navbar.appendChild(container);
    container.appendChild(logo);
    container.appendChild(toggler);
    toggler.appendChild(togglerIcon);


    document.body.appendChild(header);
}