import { createElement, removeAllChildNodes } from "../helpers/domHelper";
import Movie from "../types/Movie";
import { createMovieComponent } from "./movie";

let movies: Movie[] = [];
let filmContainer: HTMLElement;

export function createMoviesGrid(moviesList: Movie[]): HTMLElement {
    movies = moviesList;

    const container = createElement({
        tagName: 'div',
        className: 'container'
    });

    filmContainer = createElement({
        tagName: 'div',
        className: 'row',
        attributes: {
            id: 'film-container'
        }
    });

    movies.forEach(movie => renderMovie(movie));

    container.appendChild(filmContainer);

    return container;
}

export function renderMovie(movie: Movie): void {
    const movieComponent = createMovieComponent(movie, 'col-lg-3 col-md-4 col-12 p-2');
    filmContainer.appendChild(movieComponent);
}

export function renderMoreButton(onClick: () => void): void {
    const div = createElement({
        tagName: 'div',
        className: 'd-flex justify-content-center align-items-center pt-4 pb-4'
    });

    const button = createElement({
        tagName: 'button',
        className: 'btn btn-lg btn-outline-success',
        attributes: {
            id: 'load-more',
            type: 'button'
        }
    });

    button.innerText = 'Load more';
    button.addEventListener('click', onClick);

    div.appendChild(button);
    document.body.appendChild(div);
}

export function removeAllMovies(): void {
    removeAllChildNodes(filmContainer);
}

export function renderAlbum(movies: Movie[]): void {
    const album = createElement({
        tagName: 'div',
        className: 'album py-5 bg-light'
    });

    const moviesGrid = createMoviesGrid(movies);

    album.appendChild(moviesGrid);

    document.body.appendChild(album);
}