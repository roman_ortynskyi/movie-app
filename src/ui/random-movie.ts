import { createElement } from "../helpers/domHelper";
import Movie from "../types/Movie";

export function renderRandomMovie(movie: Movie): void {
    const {
        title,
        overview
    } = movie;

    const section = createElement({
        tagName: 'section',
        className: 'py-5 text-center container-fluid',
        attributes: {
            id: 'random-movie'
        }
    });

    const row = createElement({
        tagName: 'div',
        className: 'row py-lg-5'
    });

    const col = createElement({
        tagName: 'div',
        className: 'col-lg-6 col-md-8 mx-auto',
        attributes: {
            style: 'background-color: #2525254f'
        }
    });

    const name = createElement({
        tagName: 'h1',
        className: 'fw-light text-light',
        attributes: {
            id: 'random-movie-name'
        }
    });

    name.innerText = title;

    const description = createElement({
        tagName: 'p',
        className: 'lead text-white',
        attributes: {
            id: 'random-movie-description'
        }
    });

    description.innerText = overview;

    section.appendChild(row);
    row.appendChild(col);
    col.appendChild(name);
    col.appendChild(description);

    document.body.appendChild(section);
}