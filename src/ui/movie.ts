import { createElement } from "../helpers/domHelper";
import Movie from "../types/Movie";
import MovieService from "../services/MovieService";

export function createMovieComponent(movie: Movie, containerClassName: string): HTMLElement {
    const {
        overview,
        release_date,
        poster_path,
        isFavorite
    } = movie;

    const col = createElement({
        tagName: 'div',
        className: containerClassName
    });

    const card = createElement({
        tagName: 'div',
        className: 'card shadow-sm'
    });

    const imgSource = `https://image.tmdb.org/t/p/original${poster_path}`;
    const image = createElement({
        tagName: 'img',
        attributes: {
            src: imgSource
        }
    });

    const heart = document
        .getElementById('heart')
        ?.content
        ?.cloneNode(true);

    if(isFavorite) {
        heart.firstElementChild.setAttribute('fill', 'red');
    }

    const onFavoriteClick = (e: MouseEvent) => {
        const { id, isFavorite } = movie;
        const { target } = e;

        const svg = (target as HTMLElement)?.closest('svg');

        if(isFavorite) {
            MovieService.removeFromFavorite(id);
            svg?.setAttribute('fill', '#ff000078');
        }
        else {
            MovieService.addToFavorite(id);
            svg?.setAttribute('fill', 'red');
        } 
    }

    heart.firstElementChild.addEventListener('click', onFavoriteClick);

    const cardBody = createElement({
        tagName: 'div',
        className: 'card-body'
    });

    const cardText = createElement({
        tagName: 'p',
        className: 'card-text truncate'
    });

    cardText.innerText = overview;

    const dateWrapper = createElement({
        tagName: 'div',
        className: 'd-flex justify-content-between align-items-center'
    });

    const date = createElement({
        tagName: 'small',
        className: 'text-muted'
    });

    date.innerText = release_date;

    col.appendChild(card);
    card.appendChild(image);
    card.appendChild(heart);
    // card.appendChild(heartSvg);
    // heartSvg.appendChild(heartPath);
    card.appendChild(cardBody);
    cardBody.appendChild(cardText)
    cardBody.append(dateWrapper);
    dateWrapper.appendChild(date);

    return col;
}