import { createElement } from "../helpers/domHelper";
import ToggleGroupOption from "../types/ToggleGroupOption";

export function renderToggleGroup(options: ToggleGroupOption[]): void {
    const container = createElement({
        tagName: 'div',
        className: 'container-fluid d-flex bg-light justify-content-center p-2'
    });

    const buttonGroup = createElement({
        tagName: 'div',
        className: 'btn-group',
        attributes: {
            id: 'button-wrapper',
            srole: 'group',
            'aria-label': 'Basic radio toggle button group'
        }
    });

    const buttonGroupItems = options
        .map(option => [
            createButtonGroupInput(option),
            createButtonGroupLabel(option.id, option.text)
        ])
        .reduce((acc, val) => acc.concat(val, []));

    container.appendChild(buttonGroup);

    buttonGroupItems.forEach(buttonGroupItem => buttonGroup.appendChild(buttonGroupItem));
    
    document.body.appendChild(container);
}

function createButtonGroupInput(option: ToggleGroupOption) {
    const {
        id,
        onSelect
    } = option;

    const input = createElement({
        tagName: 'input',
        className: 'btn-check',
        attributes: {
            name: 'btnradio',
            id,
            autocomplete: 'off'
        }
    });

    input.addEventListener('click', onSelect);

    return input;
}

function createButtonGroupLabel(id: string, text: string) {
    const label = createElement({
        tagName: 'label',
        className: 'btn btn-outline-dark',
        attributes: {
            for: id
        }
    });

    label.innerText = text;

    return label;
}