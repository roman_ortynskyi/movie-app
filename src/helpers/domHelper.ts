interface IElement {
    tagName: string;
    className?: string;
    attributes?: Record<string, any>;
}

export function createElement(el: IElement): HTMLElement {
    const { tagName, className = '', attributes = {} } = el;
    const element = document.createElement(tagName);
  
    if (className) {
      const classNames = className.split(' ').filter(Boolean);
      element.classList.add(...classNames);
    }
  
    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
  
    return element;
}

export function removeAllChildNodes(parent: HTMLElement): void {
  while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
  }
}