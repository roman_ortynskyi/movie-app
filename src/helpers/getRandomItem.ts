function getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getRandomItem(array: any[]): any {
    const index = getRandomInt(0, array.length - 1);
    const item = array[index];

    return item;
}