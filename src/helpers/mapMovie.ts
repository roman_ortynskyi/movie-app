import MovieService from "../services/MovieService";
import Movie from "../types/Movie";
import RawMovie from "../types/RawMovie";

export function mapMovie(rawMovie: RawMovie): Movie {
    const {
        id,
        overview,
        release_date,
        poster_path,
        title
    } = rawMovie;

    const isFavorite = MovieService.isFavorite(id);

    const movie: Movie = {
        id,
        overview,
        release_date,
        poster_path,
        isFavorite,
        title
    };

    return movie;
}