import { getRandomItem } from "./helpers/getRandomItem";
import MovieService from "./services/MovieService";
import { removeAllMovies, renderAlbum, renderMoreButton, renderMovie } from "./ui/album";
import { renderFavorite } from "./ui/favorite";
import { renderHeader } from "./ui/header";
import { renderRandomMovie } from "./ui/random-movie";
import { renderToggleGroup } from "./ui/toggle-group";
import ToggleGroupOption from "./types/ToggleGroupOption";
import { renderSearchForm } from "./ui/search-form";
import Movie from './types/Movie';

let page = 1;
let category = 'popular';
let query = '';

async function showPopularMovies(): Promise<void> {
    const movies = await MovieService.getPopularMovies();
    
    removeAllMovies();
    movies.forEach(movie => renderMovie(movie));
}

async function showUpcomingMovies(): Promise<void> {
    const movies = await MovieService.getUpcomingMovies();

    removeAllMovies();
    movies.forEach(movie => renderMovie(movie));
}

async function showTopRatedMovies(): Promise<void> {
    const movies = await MovieService.getTopRatedMovies();

    removeAllMovies();
    movies.forEach(movie => renderMovie(movie));
}

export async function render(): Promise<void> {
    renderHeader();

    const favoriteMovies = await MovieService.getFavoriteMovies();

    renderFavorite(favoriteMovies);
    
    const movies = await MovieService.getPopularMovies();
    const randomMovie = getRandomItem(movies);
    renderRandomMovie(randomMovie);

    const toggleGroupOptions: ToggleGroupOption[] = [
        {
            id: 'popular',
            text: 'Popular',
            onSelect: () => {
                category = 'popular';
                showPopularMovies();
            }
        },
        {
            id: 'upcoming',
            text: 'Upcoming',
            onSelect: () => {
                category = 'upcoming';
                showUpcomingMovies();
            }
        },
        {
            id: 'top_rated',
            text: 'Top rated',
            onSelect: () => {
                category = 'top_rated';
                showTopRatedMovies();
            }
        }
    ]

    renderToggleGroup(toggleGroupOptions);
    const searchForm = renderSearchForm();

    searchForm.addEventListener('submit', async (e) => {
        e.preventDefault();
        
        category = 'search';

        query = (searchForm as HTMLFormElement).query.value;
        const movies = await MovieService.search(query, page);

        removeAllMovies();
        movies.forEach(movie => renderMovie(movie));
    })

    renderAlbum(movies);
    renderMoreButton(async () => {
        let movies: Movie[] = [];
        page++;
        switch(category) {
            case 'popular':
                movies = await MovieService.getPopularMovies(page);
                break;
            case 'upcoming':
                movies = await MovieService.getUpcomingMovies(page);
                break;
            case 'top_rated':
                movies = await MovieService.getTopRatedMovies(page);
                break;
            case 'search':
                movies = await MovieService.search(query, page);
                break;
            default:
                break;
        }

        movies.forEach(movie => renderMovie(movie));
        
    });
}
