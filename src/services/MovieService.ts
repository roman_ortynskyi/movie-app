import { apiURL, apiKey, corsAnywhere } from './config';
import { mapMovie } from '../helpers/mapMovie';
import Movie from '../types/Movie';
import RawMovie from '../types/RawMovie';

export default class MovieService {
    static async search(query: string, page: number): Promise<Movie[]> {
        const urlParams = new URLSearchParams();
        urlParams.set('query', query);
        urlParams.set('api_key', apiKey);
        urlParams.set('page', page.toString());

        const url = `${apiURL}/search/movie?${urlParams.toString()}`;

        const response = await fetch(url, {
            method: 'GET'
        });

        const { results } = await response.json();

        const movies = results.map((movie: RawMovie) => mapMovie(movie));

        return movies;
    }

    static async getPopularMovies(page = 1): Promise<Movie[]> {
        const urlParams = new URLSearchParams();
        urlParams.set('api_key', apiKey);
        urlParams.set('page', page.toString());

        const url = `${apiURL}/movie/popular?${urlParams.toString()}`;

        const response = await fetch(url, {
            method: 'GET'
        });

        const { results } = await response.json();

        const movies = results.map((movie: RawMovie) => mapMovie(movie));

        return movies;
    }

    static async getUpcomingMovies(page = 1): Promise<Movie[]> {
        const urlParams = new URLSearchParams();
        urlParams.set('api_key', apiKey);
        urlParams.set('page', page.toString());

        const url = `${apiURL}/movie/upcoming?${urlParams.toString()}`;

        const response = await fetch(url, {
            method: 'GET'
        })

        const { results } = await response.json();

        const movies = results.map((movie: RawMovie) => mapMovie(movie));

        return movies;
    }

    static async getTopRatedMovies(page = 1): Promise<Movie[]> {
        const urlParams = new URLSearchParams();
        urlParams.set('api_key', apiKey);
        urlParams.set('page', page.toString());

        const url = `${apiURL}/movie/top_rated?${urlParams.toString()}`;

        const response = await fetch(url, {
            method: 'GET'
        })

        const { results } = await response.json();

        const movies = results.map((movie: RawMovie) => mapMovie(movie));

        return movies;
    }

    static async getMovieById(id: number): Promise<Movie> {
        const urlParams = new URLSearchParams();
        urlParams.set('api_key', apiKey);

        const url = `${apiURL}/movie/${id}?${urlParams.toString()}`;

        const response = await fetch(url, {
            method: 'GET'
        })

        const data = await response.json();
        
        const movie = mapMovie(data);

        return movie;
    }

    static async getFavoriteMovies(): Promise<Movie[]> {
        const favoriteMoviesIds = this.getFavoriteMoviesIds();

        const favoriteMovies = [];
        for(const id of favoriteMoviesIds) {
            const movie = await this.getMovieById(id);
            favoriteMovies.push(movie);
        }

        return favoriteMovies;
    }

    static getFavoriteMoviesIds(): number[] {
        const currentJson = localStorage.getItem('favorite-movies') || '[]';
        const favoriteMoviesIds = JSON.parse(currentJson);

        return favoriteMoviesIds;
    } 

    static setFavoriteMoviesIds(ids: number[]): void {
        const favoriteMoviesIds = JSON.stringify(ids);
        localStorage.setItem('favorite-movies', favoriteMoviesIds);
    }

    static isFavorite(id: number): boolean {
        const favoriteMoviesIds = this.getFavoriteMoviesIds();
        const isFavorite = favoriteMoviesIds.includes(id);

        return isFavorite;
    }

    static addToFavorite(id: number): void {
        const favoriteMoviesIds = this.getFavoriteMoviesIds();
        const updated = [...favoriteMoviesIds, id];

        this.setFavoriteMoviesIds(updated);
    }

    static removeFromFavorite(id: number): void {
        const favoriteMoviesIds = this.getFavoriteMoviesIds();
        const updated = favoriteMoviesIds.filter((favoriteMovieId: number) => favoriteMovieId !== id);

        this.setFavoriteMoviesIds(updated);
    }
}