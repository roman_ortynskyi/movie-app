export default interface ToggleGroupOption {
    id: string;
    text: string;
    onSelect: () => void;
}