export default interface Movie {
    id: number;
    overview: string;
    release_date: string;
    poster_path: string;
    isFavorite?: boolean;
    title: string; 
}